# Chapter 1

## 1. Provision RHEL8 with Podman
1. Provision RHEL8.  
   I'm using Red Hat Enterprise Linux 8 (HVM), SSD Volume Type - ami-0d6ba217f554f6137 (64-bit x86) / ami-0724377cd34a397c2 (64-bit Arm) at AWS
2. Install Podman
```sh
sudo dnf module install container-tools -y
```
3. Check The podman version
```sh
podman version
```
![podman](/img/podman-version.png)

## 2. How to deploy containerized application
A containerize application is base on some images. These images are hosted in the internet on some repository. 

### i. Search for the image. 
In this example we are searching for Nginx webserver.
```sh
podman search nginx
```
<img src="/img/podman-search.png" width="800">

### ii. Pull the image
podman needs to pull the image to the local machine before instance of the image can be executed. For a first time search, podman ask to set the default repo. In this example we choose docker.io
```sh
podman pull nginx
```

<img src="/img/podman-pull-1st.png" width="400">

After the repo has been set, the search will display the images from the repo

<img src="/img/podman-pull.png" width="400">

### iii. List the pulled images
To list pulled images to the local machine 
```sh
podman images
```

<img src="/img/podman-images.png" width="400">

### iv. Run an image
To run an image is to execute an containerized application instance from the image. The format is the following

podman run [imagename]:[version] [command to be executed in the instance]

#### - existing image
The following example is to execute [echo "Hello World"] in the instance of containerized Nginx
```sh
podman run nginx:latest echo "Hello World"
``` 

<img src="/img/podman-run-nginx.png" width="400">

#### - non-existing image
If the images is not available in the local machine, podman will automatically pull the image and then runs it.
The ubi8 image ( ubi = Universal Base Image; a minimal subset of Redhat Linux OS) has not being pull before. Thus run it directly will make podman download it first and then runs it.
```sh
podman run ubi8/ubi:8.3 echo "Hello World"
```

<img src="/img/podman-run-ubi.png" width="400">

#### - run as background
To execute the image as background process, we need to pass **-d** switch . In the following example I need to specify the port expose by the nginx by the **-p** switch. 80 is the known port for Nginx. 
```sh
podman run -d -p 80 nginx:latest 
```

<img src="/img/podman-run-d.png" width="400">

To check the running background process, issue the following command
```sh
podman ps
```

<img src="/img/podman-ps.png" width="400">

From the **podman ps** output we can see that there is a local machine port which is redirected to the expose port of the containerized Nginx. We can access the Nginx through this local machine port
```sh
curl http://0.0.0.0:<local machine port>
```

<img src="/img/curl-http0000-port.png" width="400">

#### - run into the container
To get into the container as if login to the container, we can specify **-it** switch. i = interactive , t = tty a.k.a terminal like

```sh
podman run -it  nginx:latest bash 
```

<img src="/img/podman-it.png" width="400">

#### - run container with specified environtment variables
We can specify **-e** switch to specify environtment variables

```sh
podman run -e TEXT01=Hello -e TEXT02=World nginx:latest printenv TEXT01 TEXT02 
```

<img src="/img/podman-e.png" width="600">

#### - example I
- The following example will run a sample mariadb database by using the offical mariadb image https://hub.docker.com/_/mariadb

```sh
podman run -d --name mymariadb \
-e MARIADB_USER=myuser -e MARIADB_PASSWORD=myuser_pw \
-e MARIADB_DATABASE=mydatabase -e MARIADB_ROOT_PASSWORD=myroot  mariadb:latest
```

- Lets access the mariadb
```sh
podman exec -it mymariadb /bin/bash
```

- Lets login to mariadb by using the mariadb root account
```sh
mysql -umyuser -pmyuser_pw
```

- Lets list the available database
```sh
show databases;
```

- Lets focus on "mydatabase"
```sh
use mydatabase;
```

- Lets add data to "mydatabase"

create the Engineers table
```sh
CREATE TABLE Engineers (id int NOT NULL, name varchar(255) DEFAULT NULL, dept varchar(255) DEFAULT NULL, PRIMARY KEY (id));
```

add data to the Engineers table
```sh
insert into Engineers (id, name, dept) values (1,'Kapten Jeffry','IT Dept');
```

query the Engineers table
```sh
select * from Engineers;
```

<img src="/img/mariadb-example.png" width="800">

#### - example II
- The following example will run a simple httpd from quay.io with the name of **myhttpd** that will expose the httpd port 80 to the host OS port 8080

```sh
podman run -d -p 8080:80 --name myhttpd quay.io/redhattraining/httpd-parent:2.4
```

<img src="/img/podman-run-httpd.png" width="800">



to check on the httpd
```sh
podman ps | grep myhttpd
```

<img src="/img/podman-ps-httpd.png" width="800">



to access the httpd service

```sh
curl http://0.0.0.0:8080
```
<img src="/img/curl-httpd.png" width="400">



to access the httpd container
```sh
podman exec -it myhttpd /bin/bash
```

<img src="/img/podman-exect-it-httpd.png" width="400">
