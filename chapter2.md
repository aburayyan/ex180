# Chapter 2

Basic commands


## 1. To run an image
podman run imagename:version

This will run the image as foreground process and the continer will be given a random name
```sh
podman run nginx:latest
```

## 2. To check on a running processs
podman ps

*you need to open a new terminal if the foreground process still running from the step #1
```sh
podman ps
```

if to be selective on the ps output
```sh
podman ps --format="{{.ID}} {{.Names}} {{.Status}}"
```

## 3. To execute other program in the container
podman run imagename:version programname

The programname will overwrite the default program to be startup by the container image. This is know as **the entrypoint**

The following will execute the date command in the container instead of starting up the nginx deamon. 
```sh
podman run nginx:latest date
```


## 4. To execute a command in a running container
podman exec containername|containerid thecommand

```sh
podman exec mynginx cat /etc/hostname
```

## 5. To list the container
to list running container

```sh
podman ps
```

to list all container including the stopped ones
```sh
podman ps -a
```
## 6. To restart a stopped container
podman restart containername

```sh
podman restart mynginx
```

## 7. To stop the container
To gracefully stop a container
podman stop containername
```sh
podman stop containername
```

To stop all running container
```sh
podman stop -a
```

To terminate a container
```sh
podman kill containername
```



## 7. To delete a container
podman rm containername. Container must be stop to be deleted. To force delete a running container we can add -f switch 
```sh
podman rm containername
```

to delete all container
```sh
podman rm -a
```

## 8. Persistance Storage.
Follow the steps to create a host volume as the persistance storage in a container

create the directory
```sh
mkdir -p storage/mysql
```

set the application UID . Can check this at the repo. For MYSQL by RedHat the UID is 27
```sh
podman unshare chown -R 27:27 storage/mysql
```

apply the SELinux context for container for the directory. **NOTE** : directory need to be fullpath
```sh
sudo semanage fcontext -a -t container_file_t '/home/username/storage/mysql(/.*)?'
```

apply the SELinux policy on the directory. **NOTE** : directory need to be fullpath
```sh
sudo restorecon -Rv /home/username/storage/mysql
```

mount the directory as a volume in the container. **NOTE** : directory need to be fullpath
```sh
podman run -d -v /home/username/storage/mysql:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=root rhmap47/mysql 
```
