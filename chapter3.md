# Chapter 3

## 1. Accessing Redhat Catalog (RHID credentials)
1. Got to https://catalog.redhat.com 
 
2. Go to Sofware -> Container Images

3. Search the desired Image


## 2. Accessing Redhat Repo from podman

1. Login (RHID credentials)
```sh 
podman login registry.redhat.io
```

## 3. Pull image from repo

```sh
podman pull registry.redhat.io/rhel8/mysql-80
```

## 4. List available image at Podman local storage
```sh
podman images 
```

Example
```sh
[ec2-user@ip-172-31-30-36 ~]$ podman images
REPOSITORY                                TAG         IMAGE ID      CREATED        SIZE
localhost/mysql2-new                      latest      12712f2cf123  15 hours ago   373 MB
localhost/devops/mysql                    snapshot    12712f2cf123  15 hours ago   373 MB
docker.io/library/mariadb                 latest      6e0162b44a5f  2 weeks ago    421 MB
docker.io/library/nginx                   latest      f2f70adc5d89  2 weeks ago    146 MB
registry.redhat.io/rhel8/mysql-80         latest      18ec09a1f061  2 weeks ago    626 MB
registry.redhat.io/rhel8/mysql-80         1           18ec09a1f061  2 weeks ago    626 MB
registry.access.redhat.com/ubi8/ubi       8.3         613e5da7a934  11 months ago  213 MB
registry.access.redhat.com/rhmap47/mysql  latest      25e56fe83165  2 years ago    373 MB
quay.io/redhattraining/httpd-parent       2.4         3639ce1374d3  2 years ago    236 MB
```

## 5. Save an image at Podman local storage to a file
We normally do this when we need to transfer the image to another host.
podmand save -o filename.tar image-name:image-tag
The following example will save the Nginx image **docker.io/library/nginx** with the tag of **latest**  to a file called **nginx.tar**


```sh
podman save -o nginx.tar docker.io/library/nginx:latest
```

To save the image in compress form in a directory. The image will be compress by gzip
```sh
podman save --compress --format docker-dir -o mynginx docker.io/library/nginx:latest
```

## 6. To load the image to the localhost
podman load -i filename or directory

Example
```sh
[ec2-user@ip-172-31-30-36 ~]$ podman load -i mynginx.tar
Getting image source signatures
Copying blob ea4bc0cd4a93 done
Copying blob 608f3a074261 done
Copying blob ea207a4854e7 done
Copying blob 33cf1b723f65 done
Copying blob 5c77d760e1f4 done
Copying blob fac199a5a1a5 done
Copying config 12766a6745 done
Writing manifest to image destination
Storing signatures
Loaded image(s): docker.io/library/nginx:latest
[ec2-user@ip-172-31-30-36 ~]$ podman images
REPOSITORY               TAG         IMAGE ID      CREATED     SIZE
docker.io/library/nginx  latest      12766a6745ee  8 days ago  146 MB
```

## 7. To load the image to the localhost
1. To delete single image

podman rmi imagename

Example
```sh
ec2-user@ip-172-31-30-36 ~]$ podman images
REPOSITORY         TAG         IMAGE ID      CREATED     SIZE
localhost/mynginx  latest      12766a6745ee  8 days ago  146 MB
[ec2-user@ip-172-31-30-36 ~]$ podman rmi localhost/mynginx
Untagged: localhost/mynginx:latest
Deleted: 12766a6745eea133de9fdcd03ff720fa971fdaf21113d4bc72b417c123b15619
[ec2-user@ip-172-31-30-36 ~]$ podman images
REPOSITORY  TAG         IMAGE ID    CREATED     SIZE
[ec2-user@ip-172-31-30-36 ~]$
```

2. To delete all image

```sh
podman rmi -a
```

## 8. To clone a running image
```sh
podman commit running-container-name imagename:tag
```

Example
```sh
[ec2-user@ip-172-31-30-36 ~]$ podman commit mynginx mynginx-custom
Getting image source signatures
Copying blob 608f3a074261 skipped: already exists
Copying blob ea207a4854e7 skipped: already exists
Copying blob 33cf1b723f65 skipped: already exists
Copying blob 5c77d760e1f4 skipped: already exists
Copying blob fac199a5a1a5 skipped: already exists
Copying blob ea4bc0cd4a93 skipped: already exists
Copying blob 8ff3d2e47211 done
Copying config 7808730a84 done
Writing manifest to image destination
Storing signatures
7808730a84bac4b4d0d190814c12472e9fdee3ce4d91e55ec3d0db5127624f07
[ec2-user@ip-172-31-30-36 ~]$ podman ps
CONTAINER ID  IMAGE                           COMMAND               CREATED             STATUS                 PORTS       NAMES
4a750cd75547  docker.io/library/nginx:latest  nginx -g daemon o...  About a minute ago  Up About a minute ago              mynginx
[ec2-user@ip-172-31-30-36 ~]$ podman images
REPOSITORY                TAG         IMAGE ID      CREATED         SIZE
localhost/mynginx-custom  latest      7808730a84ba  25 seconds ago  146 MB
docker.io/library/nginx   latest      12766a6745ee  8 days ago      146 MB
```

## 9. To tag an existing image 
An image could be tag for having a copy with different names and Tag ( version)

podman tag exsisting-image:tag newimage-name:tag

Example
```sh
[ec2-user@ip-172-31-30-36 ~]$ podman images
REPOSITORY                TAG         IMAGE ID      CREATED      SIZE
localhost/mynginx-custom  latest      7808730a84ba  2 hours ago  146 MB
docker.io/library/nginx   latest      12766a6745ee  8 days ago   146 MB
[ec2-user@ip-172-31-30-36 ~]$ podman tag docker.io/library/nginx:latest devops/nginx:snapshot
[ec2-user@ip-172-31-30-36 ~]$ podman images
REPOSITORY                TAG         IMAGE ID      CREATED      SIZE
localhost/mynginx-custom  latest      7808730a84ba  2 hours ago  146 MB
docker.io/library/nginx   latest      12766a6745ee  8 days ago   146 MB
localhost/devops/nginx    snapshot    12766a6745ee  8 days ago   146 MB
[ec2-user@ip-172-31-30-36 ~]$
```
