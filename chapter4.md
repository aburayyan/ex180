# Chapter 4

## 1. The Containerfile
```Dockerfile
# This is a comment line
FROM ubi8/ubi:8.3
LABEL description="This is a custom httpd container image" \
      environment="Testing"
MAINTAINER Kapten Jeffry <kj@dev.io>
RUN yum update -y && \
    yum install -y httpd
EXPOSE 80
ENV LogLevel "info" \
    MYSQL_USER "myuser"
ADD http://someserver.com/filename.pdf /var/www/html
COPY ./src/ /var/www/html/
USER apache
ENTRYPOINT ["/usr/sbin/httpd"]
CMD ["-D", "FOREGROUND"]
```


### \# ( The comments)

### FROM ( reference to the parent image)

### LABEL ( Labels for the project )

### MAINTAINER ( Author's name )

### RUN ( Commands to run in the container)

### EXPOSE ( To networkd port use by the container)

### ENV ( To set environment variables)

### ADD and COPY ( Too add files to the container. ADD can do remote urls)

### USER ( users to be use in the container)

### ENTRYPOINT ( the default command to be executed when starting the container)
**if ommited the default command is /bin/sh -c**

### CMD( the default arguments for ENTRYPOINT)

## 2. Creating a custom images

### Create a working directory
```sh
mkdir -p myhttpd/src
echo "Hello World" > myhttpd/src/index.html
cd myhttpd
```

### Create the Containerfile
```sh
cat <<EOF > Containerfile
# This is a comment line
FROM ubi8/ubi:8.3
LABEL description="httpd container image" \
      environment="Testing"
MAINTAINER Kapten Jeffry <kj@dev.io>
RUN yum update -y && \
    yum install -y httpd
EXPOSE 80
ENV LogLevel "info" \
    MYSQL_USER "myuser"
ADD http://someserver.com/filename.pdf /var/www/html
COPY ./src/ /var/www/html/
USER apache
ENTRYPOINT ["/usr/sbin/httpd"]
CMD ["-D", "FOREGROUND"]
EOF
```

### Build the image
```sh
podman build -t myhttpd:1 .
```

