# Chapter 5
Openshift is a Kubernetes framework by Redhat.

<img src="/img/os-rh000.png" width="600">

TLDR;

https://cloud.redhat.com/learn/what-is-openshift?hsLang=en

## 1. Provision a Openshift Sandbox


### 1.1 Search for "try openshift" at google
<img src="/img/os-rh01.png" width="600">

### 1.2 Select the Sandbox
Select the sandbox and please fill up the required info to proceed
<img src="/img/os-rh02.png" width="600">

### 1.3 Select the Terminal **&gt;_** at the top right corner and deploy a simple nginx
<img src="/img/os-rh03.png" width="600">

```sh
kubectl run nginx --image=nginx
```

## 2. To acccess the sandbox go here
https://developers.redhat.com/developer-sandbox/get-started

Then select the sandbox access

<img src="/img/os-rh04.png" width="600">

## 3. Setup the docker hub login ( SKIP &#35;3, SOME DOCKER.IO IMAGES NOT WORKING DUE TO FILESYSTEM PERMISSION AT SANDBOX)
This is required since docker has put limit for the public download. You need to have an account at https://hub.docker.com

### 3.1 Set your Docker user and password
Change the userid and password to your own.
```sh
export DOCKERID=userid
export DOCKERPW=password
```

### 3.2 Make Openshift cluster to use your Docker credentials.
```sh
oc create secret docker-registry docker --docker-server=docker.io \
--docker-username=$DOCKERID --docker-password=$DOCKERPW \
oc secrets link default docker --for=pull 
```

### 3.3 How to deploy a container by using the Docker credentials.
oc new-app <username>/<image> --source-secret=docker

```sh
oc new-app nginx:latest --source-secret=docker
```

## 4. Create new app

This will deploy a sample nginx
```sh
oc new-app centos/nginx-112-centos7~https://github.com/sclorg/nginx-ex
```

## 5. Expose the app

### 5.1 Check the service name
```sh
oc get svc
```

Example
```sh
bash-4.4 ~ $ oc get svc
NAME                                TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)             AGE
nginx-ex                            ClusterIP   172.30.67.185    <none>        8080/TCP,8443/TCP   54s
workspace1c17985f9330416d-service   ClusterIP   172.30.119.241   <none>        4444/TCP            38h
```
### 5.2 Expose the service name
oc expose svc "the svc name"
```sh
oc expose svc nginx-ex
```

Example
```sh
bash-4.4 ~ $ oc expose svc nginx-ex
route.route.openshift.io/nginx-ex exposed
```

### 5.3 Get the route a.k.a the url
```sh
os get routes
```

Example
```sh
bash-4.4 ~ $ oc get routes
NAME       HOST/PORT                                                         PATH   SERVICES   PORT       TERMINATION   WILDCARD
nginx-ex   nginx-ex-jeffryjohar-dev.apps.sandbox.x8i5.p1.openshiftapps.com          nginx-ex   8080-tcp                 None
```

The url is accessible at browser

http://nginx-ex-jeffryjohar-dev.apps.sandbox.x8i5.p1.openshiftapps.com


<img src="/img/oc-nginx.png" width="600">

### 5.4 Accessing into the container
```sh
oc exec -it nginx-ex-7f7d8f7cf7-hznqq -- /bin/bash
```

Example
```sh
bash-4.4 ~ $ oc get pod
NAME                                         READY   STATUS             RESTARTS   AGE
nginx-112-centos7-86756bcdc9-dnltj           0/1     ImagePullBackOff   0          27m
nginx-ex-1-build                             0/1     Completed          0          24m
nginx-ex-7f7d8f7cf7-hznqq                    1/1     Running            0          19m
workspace1c17985f9330416d-556dd74787-bq9lm   2/2     Running            0          3m34s
bash-4.4 ~ $ oc exec -it nginx-ex-7f7d8f7cf7-hznqq -- /bin/bash
bash-4.2$ ls -lah /usr/share/nginx/html
total 28K
drwxr-xr-x. 2 root root   99 Nov 26  2019 .
drwxr-xr-x. 4 root root   33 Nov 26  2019 ..
-rw-r--r--. 1 root root 4.0K Aug 30  2019 404.html
-rw-r--r--. 1 root root 4.0K Aug 30  2019 50x.html
-rw-r--r--. 1 root root 4.1K Aug 30  2019 index.html
-rw-r--r--. 1 root root  368 Aug 30  2019 nginx-logo.png
-rw-r--r--. 1 root root 4.1K Aug 30  2019 poweredby.png
bash-4.2$ 
```


## 6. Inter Pods communication

### 6.1 Pod to Pod communication in the same namspace or project
Use the service name and its expose pod

Example to get the services 
```sh
bash-4.4 ~ $ oc get svc
NAME                                TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)             AGE
httpd                               ClusterIP   172.30.111.132   <none>        8080/TCP,8443/TCP   16m
mariadb                             ClusterIP   172.30.10.79     <none>        3306/TCP            16h
nginx-ex                            ClusterIP   172.30.67.185    <none>        8080/TCP,8443/TCP   17h
workspace1c17985f9330416d-service   ClusterIP   172.30.119.241   <none>        4444/TCP            2d8h
bash-4.4 ~ $
```

To access httpd just use httpd:8080. The following is a sample to curl httpd
```sh
curl http://httpd:8080
```

### 6.2 Pod to Pod communication from different namespaces or project
From different namespace, a pod can be access by the following format
**servicename.projectname.svc.cluster.local**

To access httpd from a different namespace. The following is a sample to curl httpd
```sh
curl http://httpd.jeffryjohar-dev.svc.cluster.local:8080
```
