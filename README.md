# Preparation for EX180
![podman](/img/podman-openshift.png)

EX180 is the Red Hat Certified Specialist in Containers and Kubernetes exam. This project is my study materials for the exams. This exam is basically to test the knowledge on basic podman and openshift. 

For more info about the the exam please go here:
[ex180-red-hat-certified-specialist-containers-kubernetes-exam](https://www.redhat.com/en/services/training/ex180-red-hat-certified-specialist-containers-kubernetes-exam)


## Table of Content
### 1. [Chapter 1](chapter1.md) 
Provision AWS EC2 with RHEL and Podman

Introduction to Podman

Podman Basic Commands

### 2. [Chapter 2](chapter2.md)
Persistance Storage at Podman

### 3. [Chapter 3](chapter3.md)
Image Management for Podman

### 4. [Chapter 4](chapter4.md)
Custom Image from Containerfile/Dockerfile for Podman

### 5. [Chapter 5](chapter5.md)
Provision Openshift Sandbox

Introduction to Openshift

Openshift basic commands

### 6. [Chapter 6](chapter6.md)
How to register for the exam

